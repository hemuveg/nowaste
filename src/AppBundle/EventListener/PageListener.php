<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Page as Page;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Component\DependencyInjection\Container;

class PageListener
{
  /**
   * @var Container
   */
  private $container;

  public function __construct(Container $container)
  {
    $this->container = $container;
  }

  public function onFlush(OnFlushEventArgs $args)
  {
    $em = $args->getEntityManager();
    $uow = $em->getUnitOfWork();

    foreach ( $uow->getScheduledEntityInsertions() as $entity )
    {
      if (!($entity instanceof Page)) {
        continue;
      }
      $entity = $this->changeNewEntity($em, $entity);
      $classMetadata = $em->getClassMetadata(get_class($entity));
      $uow = $em->getUnitOfWork();
      $uow->recomputeSingleEntityChangeSet($classMetadata, $entity);
      $em->persist($entity);
    }

    foreach ($uow->getScheduledEntityUpdates() as $entity) {
      if (!($entity instanceof Page)) {
        continue;
      }
      $entity = $this->changeEntity($entity);
      $classMetadata = $em->getClassMetadata(get_class($entity));
      $uow = $em->getUnitOfWork();
      $uow->recomputeSingleEntityChangeSet($classMetadata, $entity);
      $em->persist($entity);
    }
  }

  private function changeNewEntity(EntityManager $em, Page $page)
  {
    if ( !$page->getRoot() )
    {
      /* @var $repo NestedTreeRepository */
      $repo = $em->getRepository('AppBundle\Entity\Page');
      $root = $repo->getRootNodesQuery()->getOneOrNullResult();
      $page->setRoot($root);
      $page->setParent($root);
    }

    $image = $page->getHeaderImageFile();
    if ( $image )
    {
      $imagepath = realpath($image);
      $sizes = getimagesize($imagepath);
      $page->setWidth($sizes[0]);
      $page->setHeight($sizes[1]);
    }

    return $page;
  }

  private function changeEntity(Page $page)
  {
    $image = $page->getHeaderImage();
    if ( $image )
    {
      $imagefolderpath = realpath($this->container->get('kernel')->getRootDir() . '/../web/' . $this->container->getParameter('app.path.images')) . '/';
      $imagepath = $imagefolderpath . $image;
      $sizes = getimagesize($imagepath);
      $page->setWidth($sizes[0]);
      $page->setHeight($sizes[1]);
    }

    return $page;
  }
}
