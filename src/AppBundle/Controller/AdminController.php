<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{

  public function createPageListQueryBuilder(string $entityClass, string $sortDirection, $sortField)
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getRepository('AppBundle\Entity\Page')->createQueryBuilder('p');

    $qb
      ->andWhere($qb->expr()->gt($qb->getRootAlias() . '.lvl', ':minlevel'))
      ->setParameter('minlevel', 0)
      ->orderBy($qb->getRootAlias() . '.' . $sortField, $sortDirection)
    ;

    return $qb;
  }

  public function upPageAction()
  {
    $easyadmin = $this->request->attributes->get('easyadmin');
    /* @var $entity Page */
    $entity = $easyadmin['item'];
    /* @var $repo NestedTreeRepository */
    $repo = $this->getDoctrine()->getRepository(get_class($entity));
    $node = $repo->find($entity->getId());
    $repo->moveUp($node);
    $refererUrl = $this->request->query->get('referer', '');

    return !empty($refererUrl) ? $this->redirect(urldecode($refererUrl)) : $this->redirect($this->generateUrl('easyadmin', array('action' => 'list', 'entity' => $this->entity['name'])));
  }

  public function downPageAction()
  {
    $easyadmin = $this->request->attributes->get('easyadmin');
    /* @var $entity Page */
    $entity = $easyadmin['item'];
    /* @var $repo NestedTreeRepository */
    $repo = $this->getDoctrine()->getRepository(get_class($entity));
    $node = $repo->find($entity->getId());
    $repo->moveDown($node);
    $refererUrl = $this->request->query->get('referer', '');

    return !empty($refererUrl) ? $this->redirect(urldecode($refererUrl)) : $this->redirect($this->generateUrl('easyadmin', array('action' => 'list', 'entity' => $this->entity['name'])));
  }

}
