<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use DateTime;
use Doctrine\ORM\QueryBuilder;
use Swift_Attachment;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SiteController extends Controller
{

  public function indexAction(Request $request)
  {
    return $this->render('AppBundle:Site:index.html.twig');
  }

  public function headerAction(Request $request, $slug, $mainpage)
  {
    $menuitems = $this->getMenuitems($request);

    return $this->render('AppBundle:Site:header.html.twig', array(
        'menuitems' => $menuitems,
        'mainpage' => $mainpage,
        'slug' => $slug,
    ));
  }

  public function footerAction(Request $request)
  {
    $menuitems = $this->getMenuitems($request);
    $footermenuitems = $this->getMenuitems($request, true);
    return $this->render('AppBundle:Site:footer.html.twig', array(
        'menuitems' => $menuitems,
        'footermenuitems' => $footermenuitems,
    ));
  }

  public function contentboxAction($zone)
  {
    /* @var $qb QueryBuilder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    $box = $qb
        ->select('b')
        ->from('AppBundle\Entity\Contentbox', 'b')
        ->andWhere($qb->expr()->eq('b.zone', ':zone'))
        ->setParameter('zone', $zone)
        ->orderBy('b.id', 'DESC')
        ->getQuery()->getOneOrNullResult();

    $content = null;
    if ( $box )
    {
      $content = $box->getContent();

      $formPattern = "/\[\[form=([^\]]*)\]\]/";
      if ( preg_match($formPattern, $content, $m) )
      {
        $formtype = $m[1];
        $content = $this->getForm($formtype);
      }

      $modulePattern = "/\[\[box\smodule=([^\]]*)\]\]/";
      if ( preg_match($modulePattern, $content, $m) )
      {
        $module = $m[1];

        $action = 'AppBundle:Site:' . ucfirst($module) . 'box';
        return $this->forward($action);
      }

      $boxPattern = "/\[\[box\sslug=([^\]]*)\]\]/";
      if ( preg_match($boxPattern, $content, $m) )
      {
        $slug = $m[1];
        $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Page');
        /* @var $page Page */
        $page = $repo->findOneBy(array('slug' => $slug));
        if ( $page )
        {
          $template = 'AppBundle:Site:box.html.twig';
          $content = $this->container->get('twig')->render($template, array('page' => $page));
        }
      }
    }

    return $this->render('AppBundle:Site:contentbox.html.twig', array(
        'content' => $content,
    ));
  }

  public function downloadsboxAction()
  {
    $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Download');
    $items = $repo->findBy(array(), array('updated' => 'DESC'));
    return $this->render('AppBundle:Site:downloadsbox.html.twig', array(
        'items' => $items,
    ));
  }

  public function counterAction()
  {
    $startdate = new DateTime($this->getParameter('app.counter.start_date'));
    $today = new DateTime();
    $diff = $today->diff($startdate);
    $number = str_pad($this->getParameter('app.counter.start') + ($diff->format('%d') * $this->getParameter('app.counter.perday')), 7, '0', STR_PAD_LEFT);
    return $this->render('AppBundle:Site:counter.html.twig', array(
        'number' => $number,
    ));
  }

  public function pageAction(Request $request, $slug)
  {
    /* @var $qb QueryBuilderder */
    $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
    $page = $qb
      ->select('p')
      ->from('AppBundle\Entity\Page', 'p')
      ->andWhere($qb->expr()->eq('p.slug', ':slug'))
      ->setParameter('slug', $slug)
      ->getQuery()
      ->getOneOrNullResult()
    ;
    if ( !$page )
    {
      throw new NotFoundHttpException('Page not found');
    }

    $items = null;
    if ( $page->getModule() == 'download' )
    {
      $items = $this->getDoctrine()->getRepository('AppBundle\Entity\Download')->findBy(array(), array('updated' => 'DESC'));
    }

    return $this->render('AppBundle:Site:page.html.twig', array(
        'page' => $page,
        'items' => $items,
    ));
  }

  public function getForm($formtype)
  {
    $form = $this->createForm('AppBundle\\Form\\Type\\' . $formtype, null, array('label' => false));

    if ( $formtype == 'Contactform' )
    {
      $form_title = 'Ajánlatot kérek';
    }
    else
    {
      $form_title = 'Request an Offer';
    }

    return $this->container->get('twig')->render('AppBundle:Site:form.html.twig', array(
        'form' => $form->createView(),
        'form_title' => $form_title,
    ));
  }

  private $menuitems = array();

  private function getMenuitems(Request $request, $footer = false)
  {
    if ( !array_key_exists(intval($footer), $this->menuitems) )
    {
      $this->menuitems[intval($footer)] = array();
    }
    if ( !count($this->menuitems[intval($footer)]) )
    {

      $locale = $request->getLocale();
      $repo = $this->getDoctrine()->getRepository('AppBundle\Entity\Page');
      /* @var $menuqb QueryBuilder */
      $menuqb = $repo->createQueryBuilder('p');
      if ( $locale == 'hu' )
      {
        $this->menuitems[intval($footer)] = $menuqb
          ->andWhere($menuqb->expr()->gt('p.lvl', ':level'))
          ->setParameter('level', 0)
          ->andWhere($menuqb->expr()->eq('p.module', ':module'))
          ->setParameter('module', 'content')
          ->andWhere($menuqb->expr()->eq('p.footer', ':footer'))
          ->setParameter('footer', $footer)
          ->orderBy('p.lft', 'ASC')
          ->getQuery()
          ->execute()
        ;
      }
    }

    return $this->menuitems[intval($footer)];
  }

  public function formsendAction(Request $request)
  {
    $formrequest = $request->request->get('contactform');
    $files = $request->files->get('contactform');
    $message = Swift_Message::newInstance();
    $emaildata = $formrequest;
    if ( $files && array_key_exists('file', $files) )
    {
      /* @var $file UploadedFile */
      $file = $files['file'];
      $message->attach(Swift_Attachment::fromPath($file->getPathname())->setFilename($file->getClientOriginalName()));
      $emaildata['file'] = $file->getClientOriginalName();
    }
    $message
      ->setSubject('Üzenet a weboldalról')
      ->setFrom($formrequest['email'], $formrequest['name'])
      ->setTo($this->container->getParameter('app.contact_email', 'hemuveg@gmail.com'))
      ->setBody(
        $this->renderView(
          'AppBundle:Site:email.html.twig', array(
            'datas' => $emaildata,
          )
        ), 'text/html');


    $this->get('mailer')->send($message);

    return new \Symfony\Component\HttpFoundation\JsonResponse(array(
      'status' => 'ok',
      'data'   => $this->renderView('AppBundle:Site:modal.html.twig'),
    ));
  }

}
