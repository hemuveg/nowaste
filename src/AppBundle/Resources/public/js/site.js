$(function() {
  $('form[name="contactform"]').ajaxForm({
    success: function(response) {
      if ( response.status === 'ok' ) {
        $('body').append(response.data);
      }
    }
  });
});