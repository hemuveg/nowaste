<?php

namespace AppBundle\Entity;

/**
 * Contentbox
 */
class Contentbox
{

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $content;

  /**
   * @var string
   */
  private $zone;

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set content
   *
   * @param string $content
   *
   * @return Contentbox
   */
  public function setContent($content)
  {
    $this->content = $content;

    return $this;
  }

  /**
   * Get content
   *
   * @return string
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * Set zone
   *
   * @param string $zone
   *
   * @return Contentbox
   */
  public function setZone(string $zone = null)
  {
    $this->zone = $zone;

    return $this;
  }

  /**
   * Get zone
   *
   * @return string
   */
  public function getZone()
  {
    return $this->zone;
  }

  public function __toString()
  {
    return $this->getZone();
  }
}
