<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Page
 * @Vich\Uploadable
 */
class Page
{

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $title;

  /**
   * @var string
   */
  private $page_title;

  /**
   * @var string
   */
  private $module;

  /**
   * @var string
   */
  private $headerImage;

  /**
   * @Vich\UploadableField(mapping="images", fileNameProperty="headerImage")
   * @var File
   */
  private $headerImageFile;

  /**
   * @var integer
   */
  private $width;

  /**
   * @var integer
   */
  private $height;

  /**
   * @var string
   */
  private $header;

  /**
   * @var boolean
   */
  private $footer;

  /**
   * @var string
   */
  private $boxcontent;

  /**
   * @var string
   */
  private $content;

  /**
   * @var string
   */
  private $slug;

  /**
   * @var integer
   */
  private $root_id;

  /**
   * @var integer
   */
  private $parent_id;

  /**
   * @var integer
   */
  private $lft;

  /**
   * @var integer
   */
  private $rgt;

  /**
   * @var integer
   */
  private $lvl;

  /**
   * @var \DateTime
   */
  private $created;

  /**
   * @var \DateTime
   */
  private $updated;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $children;

  /**
   * @var \AppBundle\Entity\Page
   */
  private $root;

  /**
   * @var \AppBundle\Entity\Page
   */
  private $parent;

  /**
   * @var \AppBundle\Entity\Contentbox
   */
  private $right_top;

  /**
   * @var \AppBundle\Entity\Contentbox
   */
  private $right_bottom;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->children = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   *
   * @return Page
   */
  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set pageTitle
   *
   * @param string $pageTitle
   *
   * @return Page
   */
  public function setPageTitle($pageTitle)
  {
    $this->page_title = $pageTitle;

    return $this;
  }

  /**
   * Get pageTitle
   *
   * @return string
   */
  public function getPageTitle()
  {
    return $this->page_title;
  }

  /**
   * Set module
   *
   * @param string $module
   *
   * @return Page
   */
  public function setModule($module)
  {
    $this->module = $module;

    return $this;
  }

  /**
   * Get module
   *
   * @return string
   */
  public function getModule()
  {
    return $this->module;
  }

  /**
   * Set headerImage
   *
   * @param string $headerImage
   *
   * @return Page
   */
  public function setHeaderImage($headerImage)
  {
    $this->headerImage = $headerImage;

    return $this;
  }

  /**
   * Get headerImage
   *
   * @return string
   */
  public function getHeaderImage()
  {
    return $this->headerImage;
  }

  /**
   * Set width
   *
   * @param integer $width
   *
   * @return Page
   */
  public function setWidth($width)
  {
    $this->width = $width;

    return $this;
  }

  /**
   * Get width
   *
   * @return integer
   */
  public function getWidth()
  {
    return $this->width;
  }

  /**
   * Set height
   *
   * @param integer $height
   *
   * @return Page
   */
  public function setHeight($height)
  {
    $this->height = $height;

    return $this;
  }

  /**
   * Get height
   *
   * @return integer
   */
  public function getHeight()
  {
    return $this->height;
  }

  /**
   * Set header
   *
   * @param string $header
   *
   * @return Page
   */
  public function setHeader($header)
  {
    $this->header = $header;

    return $this;
  }

  /**
   * Get header
   *
   * @return string
   */
  public function getHeader()
  {
    return $this->header;
  }

  /**
   * Set footer
   *
   * @param boolean $footer
   *
   * @return Page
   */
  public function setFooter($footer)
  {
    $this->footer = $footer;

    return $this;
  }

  /**
   * Get footer
   *
   * @return boolean
   */
  public function getFooter()
  {
    return $this->footer;
  }

  /**
   * Set boxcontent
   *
   * @param string $boxcontent
   *
   * @return Page
   */
  public function setBoxcontent($boxcontent)
  {
    $this->boxcontent = $boxcontent;

    return $this;
  }

  /**
   * Get boxcontent
   *
   * @return string
   */
  public function getBoxcontent()
  {
    return $this->boxcontent;
  }

  /**
   * Set content
   *
   * @param string $content
   *
   * @return Page
   */
  public function setContent($content)
  {
    $this->content = $content;

    return $this;
  }

  /**
   * Get content
   *
   * @return string
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * Set slug
   *
   * @param string $slug
   *
   * @return Page
   */
  public function setSlug($slug)
  {
    $this->slug = $slug;

    return $this;
  }

  /**
   * Get slug
   *
   * @return string
   */
  public function getSlug()
  {
    return $this->slug;
  }

  /**
   * Set rootId
   *
   * @param integer $rootId
   *
   * @return Page
   */
  public function setRootId($rootId)
  {
    $this->root_id = $rootId;

    return $this;
  }

  /**
   * Get rootId
   *
   * @return integer
   */
  public function getRootId()
  {
    return $this->root_id;
  }

  /**
   * Set parentId
   *
   * @param integer $parentId
   *
   * @return Page
   */
  public function setParentId($parentId)
  {
    $this->parent_id = $parentId;

    return $this;
  }

  /**
   * Get parentId
   *
   * @return integer
   */
  public function getParentId()
  {
    return $this->parent_id;
  }

  /**
   * Set lft
   *
   * @param integer $lft
   *
   * @return Page
   */
  public function setLft($lft)
  {
    $this->lft = $lft;

    return $this;
  }

  /**
   * Get lft
   *
   * @return integer
   */
  public function getLft()
  {
    return $this->lft;
  }

  /**
   * Set rgt
   *
   * @param integer $rgt
   *
   * @return Page
   */
  public function setRgt($rgt)
  {
    $this->rgt = $rgt;

    return $this;
  }

  /**
   * Get rgt
   *
   * @return integer
   */
  public function getRgt()
  {
    return $this->rgt;
  }

  /**
   * Set lvl
   *
   * @param integer $lvl
   *
   * @return Page
   */
  public function setLvl($lvl)
  {
    $this->lvl = $lvl;

    return $this;
  }

  /**
   * Get lvl
   *
   * @return integer
   */
  public function getLvl()
  {
    return $this->lvl;
  }

  /**
   * Set created
   *
   * @param \DateTime $created
   *
   * @return Page
   */
  public function setCreated($created)
  {
    $this->created = $created;

    return $this;
  }

  /**
   * Get created
   *
   * @return \DateTime
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set updated
   *
   * @param \DateTime $updated
   *
   * @return Page
   */
  public function setUpdated($updated)
  {
    $this->updated = $updated;

    return $this;
  }

  /**
   * Get updated
   *
   * @return \DateTime
   */
  public function getUpdated()
  {
    return $this->updated;
  }

  /**
   * Add child
   *
   * @param \AppBundle\Entity\Page $child
   *
   * @return Page
   */
  public function addChild(\AppBundle\Entity\Page $child)
  {
    $this->children[] = $child;

    return $this;
  }

  /**
   * Remove child
   *
   * @param \AppBundle\Entity\Page $child
   */
  public function removeChild(\AppBundle\Entity\Page $child)
  {
    $this->children->removeElement($child);
  }

  /**
   * Get children
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getChildren()
  {
    return $this->children;
  }

  /**
   * Set root
   *
   * @param \AppBundle\Entity\Page $root
   *
   * @return Page
   */
  public function setRoot(\AppBundle\Entity\Page $root = null)
  {
    $this->root = $root;

    return $this;
  }

  /**
   * Get root
   *
   * @return \AppBundle\Entity\Page
   */
  public function getRoot()
  {
    return $this->root;
  }

  /**
   * Set parent
   *
   * @param \AppBundle\Entity\Page $parent
   *
   * @return Page
   */
  public function setParent(\AppBundle\Entity\Page $parent = null)
  {
    $this->parent = $parent;

    return $this;
  }

  /**
   * Get parent
   *
   * @return \AppBundle\Entity\Page
   */
  public function getParent()
  {
    return $this->parent;
  }

  /**
   * Set rightTop
   *
   * @param \AppBundle\Entity\Contentbox $rightTop
   *
   * @return Page
   */
  public function setRightTop(\AppBundle\Entity\Contentbox $rightTop = null)
  {
    $this->right_top = $rightTop;

    return $this;
  }

  /**
   * Get rightTop
   *
   * @return \AppBundle\Entity\Contentbox
   */
  public function getRightTop()
  {
    return $this->right_top;
  }

  /**
   * Set rightBottom
   *
   * @param \AppBundle\Entity\Contentbox $rightBottom
   *
   * @return Page
   */
  public function setRightBottom(\AppBundle\Entity\Contentbox $rightBottom = null)
  {
    $this->right_bottom = $rightBottom;

    return $this;
  }

  /**
   * Get rightBottom
   *
   * @return \AppBundle\Entity\Contentbox
   */
  public function getRightBottom()
  {
    return $this->right_bottom;
  }

  public function getHeaderImageFile()
  {
    return $this->headerImageFile;
  }

  public function setHeaderImageFile(File $header_image_file = null)
  {
    $this->headerImageFile = $header_image_file;
    if ($header_image_file) {
      $this->setUpdated(new \DateTime('now'));
    }

    return $this;
  }

}
