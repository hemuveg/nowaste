<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactformEnglish extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('name', TextType::class, array(
        'label'    => 'Name',
        'required' => true,
      ))
      ->add('email', EmailType::class, array(
        'label'    => 'Email',
        'required' => true,
      ))
      ->add('msg', TextareaType::class, array(
        'label'    => 'Comment',
        'required' => true,
        'attr'     => array(
          'rows'    => 3,
        )
      ))
      ->add('tel', TextType::class, array(
        'label'    => 'Phone',
        'required' => false,
      ))
      ->add('file', FileType::class, array(
        'label'    => 'Attach file',
        'required' => false,
      ))
      ->add('newsletter', CheckboxType::class, array(
        'label'    => 'Subscribe to Newsletter',
        'required' => false,
        'data'     => true,
      ))
      ->add('send', SubmitType::class, array(
        'label' => 'Send',
        'attr'  => array(
          'class' => 'nowaste-btn',
        ),
      ))
    ;
  }

}